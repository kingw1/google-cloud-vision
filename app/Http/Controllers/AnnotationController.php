<?php

namespace App\Http\Controllers;

use App\Data;
use GoogleCloudVision\GoogleCloudVision;
use GoogleCloudVision\Request\AnnotateImageRequest;
use Illuminate\Http\Request;

class AnnotationController extends Controller
{
    public function displayForm()
    {
        $datas = Data::orderBy('created_at', 'desc')->limit(5)->get();

        return view('annotate', compact('datas'));
    }

    public function annotateImage(Request $request)
    {
        if ($request->file('image')) {
            //convert image to base64
            $image = base64_encode(file_get_contents($request->file('image')));

            //prepare request
            $request = new AnnotateImageRequest();
            $request->setImage($image);
            $request->setFeature("DOCUMENT_TEXT_DETECTION");
            $gcvRequest = new GoogleCloudVision([$request], env('GOOGLE_CLOUD_API_KEY'));

            //send annotation request
            $response = $gcvRequest->annotate();
            if (is_object($response->responses[0]) && isset($response->responses[0]->textAnnotations[0])) {
                $description = $response->responses[0]->textAnnotations[0]->description;
                Data::create([
                    'description' => $description,
                ]);
            }

            return redirect()->back();
        }
    }

    public function saveImage()
    {
        $imageName = uniqid() . '.png';
        $image = \Image::make(request()->get('imgBase64'));
        $image->save(public_path('img/' . $imageName));

        $this->imageDetection($imageName);

        return [
            'result' => 1,
            'message' => 'บันทึกเรียบร้อยแล้ว',
            'file' => $imageName,
        ];
    }

    private function imageDetection($imageName)
    {
        $image = base64_encode(file_get_contents(public_path('img/' . $imageName)));

        //prepare request
        $request = new AnnotateImageRequest();
        $request->setImage($image);
        $request->setFeature("DOCUMENT_TEXT_DETECTION");
        $gcvRequest = new GoogleCloudVision([$request], env('GOOGLE_CLOUD_API_KEY'));

        //send annotation request
        $response = $gcvRequest->annotate();
        if (is_object($response->responses[0]) && isset($response->responses[0]->textAnnotations[0])) {
            $description = $response->responses[0]->textAnnotations[0]->description;
            Data::create([
                'description' => $description,
            ]);

            \File::delete(public_path('img/' . $imageName));
        }
    }
}
