<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Jndweb Image Text Detection</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    </head>
    <body>
      <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="#">Image Text Detection</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
      </nav>

      <div class="container">
          <div class="row justify-content-center">
              <div class="col-md-8">
                  {{-- <div class="card" style="margin-top: 30px;">
                      <div class="card-header">Upload Image</div>
                      <div class="card-body">
                          <form role="form" method="POST" action="/annotate" enctype="multipart/form-data">
                              @csrf
                              <div class="form-group row">
                                  <label for="email" class="col-sm-4 col-form-label text-md-right">{{ __('Upload image') }}</label>

                                  <div class="col-md-6">
                                      <input id="image" type="file" class="form-control{{ $errors->has('image') ? ' is-invalid' : '' }}" name="image" value="{{ old('image') }}" required autofocus>

                                      @if ($errors->has('image'))
                                          <span class="invalid-feedback">
                                              <strong>{{ $errors->first('image') }}</strong>
                                          </span>
                                      @endif
                                  </div>
                              </div>
                              <div class="form-group row mb-0">
                                  <div class="col-md-8 offset-md-4">
                                      <button type="submit" class="btn btn-primary">
                                          Upload
                                      </button>
                                  </div>
                              </div>
                          </form>
                      </div>
                  </div> --}}

                  <div class="card" style="margin-top: 30px;">
                      <div class="card-header">Draw your text</div>
                      <div class="card-body">
                            <div id="signature-pad" class="m-signature-pad" style="background-color: #eee;">
                                <div class="m-signature-pad--body">
                                    <canvas height="300"></canvas>
                                </div>
                            </div>
                            <div style="margin-top: 20px;">
                                <input type="button" class="btn btn-primary" id="submitButton" value="Save" />
                                <button type="button" class="btn btn-secondary btn-clear-pad" data-action="clear">Clear</button>
                            </div>
                      </div>
                  </div>

                  <div class="card" style="margin-top: 30px;">
                        <div class="card-header">
                            Detect Text Result
                        </div>
                        <div class="card-body">
                            <table class="table table-bordered">
                                @foreach ($datas as $r)
                                <tr>
                                    <td>
                                        Created At: {{ $r->created_at }}
                                        <textarea class="form-control">{!! $r->description !!}</textarea>
                                    </td>
                                </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
              </div>
          </div>
      </div>
    </body>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
	<script type="text/javascript" src="{{ asset('vendor/signature_pad.min.js') }}"></script>
    <script>
        $(function() {
            var wrapper = document.getElementById("signature-pad"),
                canvas = wrapper.querySelector("canvas"),
                signaturePad;

            var parentWidth = $(canvas).parent().outerWidth();
            canvas.setAttribute("width", parentWidth);

            signaturePad = new SignaturePad(canvas);

            $('input#submitButton').click( function() {
                var dataURL = signaturePad.toDataURL('image/png');

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: "POST",
                    url: "/save-image",
                    data: {
                        imgBase64: dataURL
                    }
                })
                .done(function(data) {
                    if (data.result == 1) {
                        alert(data.message);
                        window.location.reload();
                    }
                });
            });

            $('.btn-clear-pad').click(function() { signaturePad.clear() });
        });
    </script>
</html>